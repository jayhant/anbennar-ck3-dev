k_eborthil = {
	1000.1.1 = {
		change_development_level = 7
	}
	890.8.25 = {
		holder = taefdares_0001
	}
	930.7.16 = {
		holder = taefdares_0002
	}
	955.1.3 = {
		holder = taefdares_0005
	}
	958.11.5 = {
		holder = taefdares_0006
	}
	982.9.12 = {
		holder = taefdares_0007
	}
	986.6.3 = {
		holder = taefdares_0011
	}
	1007.10.3 = {
		holder = taefdares_0007
	}
	1018.11.6 = {
		holder = 17 #Eborian Goldwater
	}
}

c_eborthil = {
	1000.1.1 = {
		change_development_level = 13
	}
	890.8.25 = {
		holder = taefdares_0001
	}
	930.7.16 = {
		holder = taefdares_0002
	}
	955.1.3 = {
		holder = taefdares_0005
	}
	958.11.5 = {
		holder = taefdares_0006
	}
	982.9.12 = {
		holder = taefdares_0007
	}
	986.6.3 = {
		holder = taefdares_0011
	}
	1007.10.3 = {
		holder = taefdares_0007
	}
	1018.11.6 = {
		holder = 17 #Eborian Goldwater
	}
}

c_port_jurith = {
	1000.1.1 = {
		change_development_level = 10
	}
	940.3.21 = {
		holder = luminares_0004
	}
	957.5.11 = {
		holder = luminares_0005
	}
	991.9.28 = {
		holder = luminares_0009
		}
	1020.6.7 = {
		holder = luminares_0011
		liege = k_eborthil
	}
}

c_toref_citadel = {
	1000.1.1 = {
		change_development_level = 9
	}
	986.6.3 = {
		holder = taefdares_0011
	}
	1002.7.19 = {
		holder = luminares_0009
	}
	1020.6.7 = {
		holder = luminares_0011
		liege = k_eborthil
	}
}

c_stonegaze = {
	1000.1.1 = {
		change_development_level = 11
	}
	958.11.5 = {
		holder = taefdares_0010
	}
	976.9.26 = {
		holder = taefdares_0011
	}
	1007.10.3 = {
		holder = taefdares_0007
	}
	1018.11.6 = {
		holder = 17
	}
}

c_new_tefkora = {
	1000.1.1 = {
		change_development_level = 11
	}
	910.6.13 = {
		holder = nirhaunes_0002
		liege = k_eborthil
	}
	939.9.6 = {
		holder = nirhaunes_0003
	}
	972.1.31 = {
		holder = nirhaunes_0004
	}
	986.9.6 = {
		holder = nirhaunes_0005
	}
}
