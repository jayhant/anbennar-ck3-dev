#k_ourdia
##d_tencmarck
###c_bal_ouord
510 = {		#Bal Ouord

    # Misc
    culture = ourdi
    religion = southern_cult_of_castellos
	holding = castle_holding

    # History
	1000.1.1 = {
		special_building_slot = castanorian_citadel_bal_ouord_01
		special_building = castanorian_citadel_bal_ouord_01
	}
}
2816 = {

    # Misc
    holding = city_holding

    # History

}
2817 = {

    # Misc
    holding = church_holding

    # History

}
2818 = {

    # Misc
    holding = none

    # History

}

###c_grenwoud
511 = {		#Grenwoud

    # Misc
    culture = ourdi
    religion = southern_cult_of_castellos
	holding = castle_holding

    # History
}
2819 = {

    # Misc
    holding = city_holding

    # History

}
2820 = {

    # Misc
    holding = none

    # History

}

###c_aldwigard
509 = {		#Aldwigard

    # Misc
    culture = ourdi
    religion = southern_cult_of_castellos
	holding = castle_holding

    # History
}
2821 = {

    # Misc
    holding = church_holding

    # History

}
2822 = {

    # Misc
    holding = none

    # History

}

##d_lencmarck
###c_blachyl
506 = {		#Blachyl

    # Misc
    culture = ourdi
    religion = southern_cult_of_castellos
	holding = castle_holding

    # History
}
2804 = {

    # Misc
    holding = none

    # History

}
2805 = {

    # Misc
    holding = city_holding

    # History

}
2806 = {

    # Misc
    holding = none

    # History

}

###c_aldandhol
507 = {		#Aldandhol

    # Misc
    culture = ourdi
    religion = southern_cult_of_castellos
	holding = castle_holding

    # History
}
2807 = {

    # Misc
    holding = church_holding

    # History

}
2808 = {

    # Misc
    holding = none

    # History

}

###c_landsend
508 = {		#Landsend

    # Misc
    culture = ourdi
    religion = southern_cult_of_castellos
	holding = castle_holding

    # History
}
2809 = {

    # Misc
    holding = none

    # History

}

##d_oudmarck
###c_withacen
515 = {		#Withacen

    # Misc
    culture = ourdi
    religion = southern_cult_of_castellos
	holding = castle_holding

    # History
}
2810 = {

    # Misc
    holding = city_holding

    # History

}
2811 = {

    # Misc
    holding = none

    # History

}

###c_madiroud
514 = {		#Madiroud

    # Misc
    culture = ourdi
    religion = southern_cult_of_castellos
	holding = castle_holding

    # History
}
2812 = {

    # Misc
    holding = none

    # History

}

###c_southtower
513 = {		#Southtower

    # Misc
    culture = ourdi
    religion = southern_cult_of_castellos
	holding = castle_holding

    # History
}
2813 = {

    # Misc
    holding = church_holding

    # History

}

###c_fatherwell
512 = {		#Fatherwell

    # Misc
    culture = ourdi
    religion = southern_cult_of_castellos
	holding = castle_holding

    # History
}
2814 = {

    # Misc
    holding = city_holding

    # History

}
2815 = {

    # Misc
    holding = none

    # History

}