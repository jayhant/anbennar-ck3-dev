﻿#k_cast

##d_trialmount
###c_the_north_citadel
2446 = {	#The North Citadel

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
	1000.1.1 = {
		special_building_slot = castanorian_citadel_north_citadel_01
		special_building = castanorian_citadel_north_citadel_01
	}
}
840 = { #prob castellos temple to go up the trialmount stuff

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = church_holding

	# History
	1000.1.1 = {
		special_building_slot = castanorian_citadel_north_citadel_01
		special_building = castanorian_citadel_north_citadel_01
	}

}
2510 = {

    # Misc
    holding = none

    # History

}
2445 = {

    # Misc
    holding = none

    # History

}

###c_glory_road
847 = {		#Glory Road

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2501 = {

    # Misc
    holding = church_holding

    # History

}

###c_victors_path
845 = {		#Victor's Path

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2500 = {

    # Misc
    holding = none

    # History

}

###c_trialwood
843 = { 	#Trialwood

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2499 = {

    # Misc
    holding = none

    # History

}

##d_sapphirecrown
###c_the_south_citadel
2424 = {

    # Misc
    culture = castanorian
    religion = castanorian_pantheon
	holding = castle_holding

    # History
	1000.1.1 = {
		special_building_slot = castanorian_citadel_south_citadel_01
		special_building = castanorian_citadel_south_citadel_01
	}
}

###c_test_2427
2427 = {

    # Misc
    culture = stone_dwarvish
    religion = cult_of_balgar
	holding = city_holding

    # History

}
2428 = {

	# Misc
	holding = none

	# History
}

###c_sapphirewatch
834 = {	

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2429 = {

    # Misc
    holding = city_holding

    # History

}
2430 = {

    # Misc
    holding = church_holding

    # History

}
2431 = {

    # Misc
    holding = none

    # History

}

##d_nortmere
###c_nortmerewic
835 = {		#Nortmerewic

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2417 = {

    # Misc
    holding = none

    # History

}
2418 = {

    # Misc
    holding = city_holding

    # History

}
2419 = {

    # Misc
    holding = none

    # History

}

###c_wardenhall
755 = {		#Wardenhall

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2420 = {

    # Misc
    holding = none

    # History

}
2421 = {

    # Misc
    holding = church_holding

    # History

}

###c_nurced
836 = {		#Nurced

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2422 = {

    # Misc
    holding = city_holding

    # History

}
2423 = {

    # Misc
    holding = none

    # History

}

##d_serpentsmarck
###c_serpentswic
846 = {		#Serpentswic

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2498 = {

    # Misc
    holding = city_holding

    # History

}
2503 = {

    # Misc
    holding = none

    # History

}

###c_nortmarck
852 = {		#Nortmarck

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2504 = {

    # Misc
    holding = none

    # History

}
2505 = {

    # Misc
    holding = none

    # History

}

###c_shatterwood
858 = {		#Shatterwood

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2506 = {

    # Misc
    holding = city_holding

    # History

}
2507 = {

    # Misc
    holding = none

    # History

}
2508 = {

    # Misc
    holding = none

    # History

}

###c_charwic
859 = {		#Charwic

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2509 = {

    # Misc
    holding = none

    # History

}

##d_northyl
###c_nortwood
743 = {		#Nortwood

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2410 = {

    # Misc
    holding = none

    # History

}
2411 = {

    # Misc
    holding = none

    # History

}

###c_acenort
746 = {		#Acenort

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2412 = {

    # Misc
    holding = church_holding

    # History

}

###c_hunters_folly
850 = {		#Hunters_Folly

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2413 = {

    # Misc
    holding = city_holding

    # History

}
2414 = {

    # Misc
    holding = none

    # History

}

###c_khugsroad
849 = {		#Khugsroad

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2415 = {

    # Misc
    holding = none

    # History

}
2416 = {

    # Misc
    holding = city_holding

    # History

}

##d_westgate
###c_westgate
754 = {		#Westgate

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
	1000.1.1 = {
		special_building_slot = white_walls_of_castanor_01
		special_building = white_walls_of_castanor_01
	}
}
2432 = {

    # Misc
    holding = city_holding

    # History

}
2433 = {

    # Misc
    holding = church_holding

    # History

}

###c_venacvord
842 = {		#Venacvord

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2434 = {

    # Misc
    holding = none

    # History

}
2435 = {

    # Misc
    holding = church_holding

    # History

}

###c_rigelham
747 = {		#Rigelham

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2436 = {

    # Misc
    holding = city_holding

    # History

}

###c_westwood
848 = {		#Westwood

	# Misc
	culture = black_castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2437 = {

    # Misc
    holding = none

    # History

}
2438 = {

    # Misc
    holding = none

    # History

}

##d_nath
###c_caylensfort
837 = {		#Caylensfort

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
2442 = {

    # Misc
    holding = church_holding

    # History

}
2443 = {

    # Misc
    holding = city_holding

    # History

}

###c_aldenmore
2439 = {

    # Misc
    culture = castanorian
    religion = castanorian_pantheon
	holding = castle_holding

    # History

}
2440 = {

    # Misc
    holding = none

    # History

}
2441 = {

    # Misc
    holding = church_holding

    # History

}

###c_elderlan
2444 = {		#Elderlan

	# Misc
	culture = castanorian
	religion = castanorian_pantheon
	holding = castle_holding

	# History
}
839 = {

    # Misc
    holding = city_holding

    # History

}
2497 = {

    # Misc
    holding = none

    # History

}
2502 = {

    # Misc
    holding = church_holding

    # History

}
