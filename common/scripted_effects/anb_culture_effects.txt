﻿setup_bilingual_traditions = {
	every_culture_global = {
		set_culture_languages_from_traditions = yes
	}
}

set_culture_languages_from_traditions = {
	clear_variable_list = cultural_languages

	if = {
		limit = {
			has_cultural_tradition = tradition_language_gnomish_lencori
		}

		save_language_to_culture = {
			language = culture:creek_gnomish
		}
		save_language_to_culture = {
			language = culture:lorentish
		}
	}

	if = {
		limit = {
			has_cultural_tradition = tradition_language_castanorian_dwarven
		}

		save_language_to_culture = {
			language = culture:castanorian
		}
		save_language_to_culture = {
			language = culture:stone_dwarvish
		}
	}

	if = {
		limit = {
			has_cultural_tradition = tradition_language_copper_dwarven_bahari
		}

		save_language_to_culture = {
			language = culture:yametsesi
		}
		save_language_to_culture = {
			language = culture:copper_dwarvish
		}
	}
}

save_language_to_culture = {
	add_to_variable_list = {
		name = cultural_languages
		target = $language$ 
	}
}

save_language_to_learn_from_culture = {
	random_in_list = {
		variable = cultural_languages
		limit = {
			NOT = {
				scope:character = {
					knows_language_of_culture = PREV
				}
			}
		}
		
		save_scope_as = culture_to_learn_from
	}
}
