﻿cannorian_pantheon_religion = {
	family = rf_pantheonic
	graphical_faith = dharmic_gfx
	doctrine = pantheonic_hostility_doctrine 

	#Main Group
	doctrine = doctrine_no_head	
	doctrine = doctrine_gender_male_dominated	#tjhis is standard castanorian style. other faiths of this religion may be different, for example The Dame
	doctrine = doctrine_pluralism_pluralistic
	doctrine = doctrine_theocracy_temporal

	#Marriage
	doctrine = doctrine_monogamy
	doctrine = doctrine_divorce_approval
	doctrine = doctrine_bastardry_legitimization
	doctrine = doctrine_consanguinity_cousins

	#Crimes
	doctrine = doctrine_homosexuality_shunned
	doctrine = doctrine_adultery_men_shunned
	doctrine = doctrine_adultery_women_crime
	doctrine = doctrine_kinslaying_extended_family_crime
	doctrine = doctrine_deviancy_crime
	doctrine = doctrine_witchcraft_crime

	#Clerical Functions
	doctrine = doctrine_clerical_function_taxation
	doctrine = doctrine_clerical_gender_either
	doctrine = doctrine_clerical_marriage_allowed
	doctrine = doctrine_clerical_succession_spiritual_appointment
	
	traits = {	
			virtues = {
				just
				}
	
			sins = {
				arbitrary	
				}
	}
	
	#Allow pilgrimages
	doctrine = doctrine_pilgrimage_encouraged
	

	custom_faith_icons = {	#TODO
		custom_faith_1 custom_faith_2 custom_faith_3 custom_faith_4 custom_faith_5 custom_faith_6 custom_faith_7 custom_faith_8 custom_faith_9 custom_faith_10 dualism_custom_1 zoroastrian_custom_1 zoroastrian_custom_2 buddhism_custom_1 buddhism_custom_2 buddhism_custom_3 buddhism_custom_4 taoism_custom_1 yazidi_custom_1 sunni_custom_2 sunni_custom_3 sunni_custom_4 ibadi_custom muhakkima_1 muhakkima_2 muhakkima_4 muhakkima_5 muhakkima_6 judaism_custom_1
	}

	holy_order_names = {	#TODO
		{ name = "holy_order_followers_of_arjuna" }
		{ name = "holy_order_faith_maharatas" }
		{ name = "holy_order_vyuha_of_highgod" }
		{ name = "holy_order_vyuha_of_the_temple_of_place" }
		{ name = "holy_order_maharatas_of_highgod" }
	}

	holy_order_maa = { praetorian }	#their men at arms

	localization = {
		#HighGod - Castellos
		HighGodName = cannorian_pantheon_high_god_name
		HighGodNamePossessive = cannorian_pantheon_high_god_name_possessive
		HighGodNameSheHe = CHARACTER_SHEHE_IT
		HighGodHerselfHimself = CHARACTER_ITSELF
		HighGodHerHis = CHARACTER_HERHIS_ITS
		HighGodNameAlternate = cannorian_pantheon_high_god_name_alternate
		HighGodNameAlternatePossessive = cannorian_pantheon_high_god_name_alternate_possessive

		#Creator
		CreatorName = cannorian_pantheon_creator_god_name
		CreatorNamePossessive = cannorian_pantheon_creator_god_name_possessive
		CreatorSheHe = CHARACTER_SHEHE_HE
		CreatorHerHis = CHARACTER_HERHIS_HIS
		CreatorHerHim = CHARACTER_HERHIM_HIM

		#HealthGod
		HealthGodName = cannorian_pantheon_health_god_name
		HealthGodNamePossessive = cannorian_pantheon_health_god_name_possessive
		HealthGodSheHe = CHARACTER_SHEHE_SHE
		HealthGodHerHis = CHARACTER_HERHIS_HER
		HealthGodHerHim = CHARACTER_HERHIM_HER
		
		#FertilityGod
		FertilityGodName = cannorian_pantheon_fertility_god_name
		FertilityGodNamePossessive = cannorian_pantheon_fertility_god_name_possessive
		FertilityGodSheHe = CHARACTER_SHEHE_SHE
		FertilityGodHerHis = CHARACTER_HERHIS_HER
		FertilityGodHerHim = CHARACTER_HERHIM_HER

		#WealthGod
		WealthGodName = cannorian_pantheon_wealth_god_name
		WealthGodNamePossessive = cannorian_pantheon_wealth_god_name_possessive
		WealthGodSheHe = CHARACTER_SHEHE_HE
		WealthGodHerHis = CHARACTER_HERHIS_HIS
		WealthGodHerHim = CHARACTER_HERHIM_HIM

		#HouseholdGod
		HouseholdGodName = cannorian_pantheon_household_god_name
		HouseholdGodNamePossessive = cannorian_pantheon_household_god_name_possessive
		HouseholdGodSheHe = CHARACTER_SHEHE_SHE
		HouseholdGodHerHis = CHARACTER_HERHIS_HER
		HouseholdGodHerHim = CHARACTER_HERHIM_HER

		#FateGod
		FateGodName = cannorian_pantheon_fate_god_name
		FateGodNamePossessive = cannorian_pantheon_fate_god_name_possessive
		FateGodSheHe = CHARACTER_SHEHE_IT
		FateGodHerHis = CHARACTER_HERHIS_ITS
		FateGodHerHim = CHARACTER_HERHIM_IT

		#KnowledgeGod
		KnowledgeGodName = cannorian_pantheon_knowledge_god_name
		KnowledgeGodNamePossessive = cannorian_pantheon_knowledge_god_name_possessive
		KnowledgeGodSheHe = CHARACTER_SHEHE_SHE
		KnowledgeGodHerHis = CHARACTER_HERHIS_HER
		KnowledgeGodHerHim = CHARACTER_HERHIM_HER

		#WarGod
		WarGodName = cannorian_pantheon_war_god_name
		WarGodNamePossessive = cannorian_pantheon_war_god_name_possessive
		WarGodSheHe = CHARACTER_SHEHE_SHE
		WarGodHerHis = CHARACTER_HERHIS_HER
		WarGodHerHim = CHARACTER_HERHIM_HER

		#TricksterGod
		TricksterGodName = cannorian_pantheon_trickster_god_name
		TricksterGodNamePossessive = cannorian_pantheon_trickster_god_name_possessive
		TricksterGodSheHe = CHARACTER_SHEHE_HE
		TricksterGodHerHis = CHARACTER_HERHIS_HIS
		TricksterGodHerHim = CHARACTER_HERHIM_HIM

		#NightGod
		NightGodName = cannorian_pantheon_night_god_name
		NightGodNamePossessive = cannorian_pantheon_night_god_name_possessive
		NightGodSheHe = CHARACTER_SHEHE_SHE
		NightGodHerHis = CHARACTER_HERHIS_HER
		NightGodHerHim = CHARACTER_HERHIM_HER

		#WaterGod
		WaterGodName = cannorian_pantheon_water_god_name
		WaterGodNamePossessive = cannorian_pantheon_water_god_name_possessive
		WaterGodSheHe = CHARACTER_SHEHE_SHE
		WaterGodHerHis = CHARACTER_HERHIS_HER
		WaterGodHerHim = CHARACTER_HERHIM_HER


		PantheonTerm = religion_the_gods
		PantheonTermHasHave = pantheon_term_have
		GoodGodNames = {
			cannorian_pantheon_high_god_name
			cannorian_pantheon_high_god_name_alternate
			cannorian_pantheon_water_god_name
			cannorian_pantheon_knowledge_god_name
			cannorian_pantheon_war_god_name
			cannorian_pantheon_household_god_name
			cannorian_pantheon_fate_god_name
			cannorian_pantheon_fertility_god_name
			cannorian_pantheon_health_god_name
		}
		
		
		DevilName = cannorian_pantheon_devil_name
		DevilNamePossessive = cannorian_pantheon_devil_name_possessive
		DevilSheHe = CHARACTER_SHEHE_THEY
		DevilHerHis = CHARACTER_HERHIS_THEIR
		DevilHerselfHimself = cannorian_pantheon_devil_herselfhimself
		EvilGodNames = {
			cannorian_pantheon_devil_name
			cannorian_pantheon_evil_god_archdevils
			cannorian_pantheon_evil_god_agrados
		}
		HouseOfWorship = cannorian_pantheon_house_of_worship
		HouseOfWorshipPlural = cannorian_pantheon_house_of_worship_plural
		ReligiousSymbol = cannorian_pantheon_religious_symbol
		ReligiousText = cannorian_pantheon_religious_text
		ReligiousHeadName = cannorian_pantheon_religious_head_title
		ReligiousHeadTitleName = cannorian_pantheon_religious_head_title_name
		DevoteeMale = cannorian_pantheon_devotee_male
		DevoteeMalePlural = cannorian_pantheon_devotee_male_plural
		DevoteeFemale = cannorian_pantheon_devotee_female
		DevoteeFemalePlural = cannorian_pantheon_devotee_female_plural
		DevoteeNeuter = cannorian_pantheon_devotee_neuter
		DevoteeNeuterPlural = cannorian_pantheon_devotee_neuter_plural
		PriestMale = cannorian_pantheon_priest
		PriestMalePlural = cannorian_pantheon_priest_plural
		PriestFemale = cannorian_pantheon_priest
		PriestFemalePlural = cannorian_pantheon_priest_plural
		PriestNeuter = cannorian_pantheon_priest
		PriestNeuterPlural = cannorian_pantheon_priest_plural
		AltPriestTermPlural = cannorian_pantheon_priest_term_plural
		BishopMale = cannorian_pantheon_bishop
		BishopMalePlural = cannorian_pantheon_bishop_plural
		BishopFemale = cannorian_pantheon_bishop
		BishopFemalePlural = cannorian_pantheon_bishop_plural
		BishopNeuter = cannorian_pantheon_bishop
		BishopNeuterPlural = cannorian_pantheon_bishop_plural
		DivineRealm = cannorian_pantheon_divine_realm
		PositiveAfterLife = cannorian_pantheon_positive_afterlife
		NegativeAfterLife = cannorian_pantheon_negative_afterlife
		DeathDeityName = cannorian_pantheon_death_name
		DeathDeityNamePossessive = cannorian_pantheon_death_name_possessive
		DeathDeitySheHe = CHARACTER_SHEHE_HE
		DeathDeityHerHis = CHARACTER_HERHIS_HIS
		WitchGodName = cannorian_pantheon_devil_name
		WitchGodHerHis = CHARACTER_HERHIS_HIS
		WitchGodSheHe = CHARACTER_SHEHE_HE
		WitchGodHerHim = CHARACTER_HERHIM_HIM
		WitchGodMistressMaster = master
		WitchGodMotherFather = father

		GHWName = ghw_purification
		GHWNamePlural = ghw_purifications
	}

	faiths = {
		cult_of_the_dame = {
			color = { 45 151 178 }
			religious_head = d_highest_moon
			icon = temple_of_the_highest_moon
			
			holy_site = anbenncost
			holy_site = moonmount

			doctrine = tenet_astrology	#stars and moons man
			doctrine = tenet_ritual_celebrations	#the dame was the goddess of agriculture. feasts. also Lansday
			doctrine = tenet_esotericism	#wise dudes
			
			#Main Group
			doctrine = doctrine_spiritual_head
			doctrine = doctrine_gender_equal	

			doctrine = doctrine_homosexuality_accepted
			#doctrine = doctrine_pluralism_pluralistic	#defaults
			#doctrine = doctrine_theocracy_lay_clergy # You can't have this commented out and temporal head activated at the same time


			#Clerical Functions
			doctrine = doctrine_clerical_function_alms_and_pacification
			doctrine = doctrine_clerical_gender_female_only

			doctrine = doctrine_witchcraft_crime	#For Testing 

			localization = {
				ReligiousHeadName = luminary_title
			}
		}

		whiterose_court = {
			color = { 229 142 151 }
			religious_head = d_highcour
			icon = the_dame
			
			holy_site = lorentaine
			holy_site = minar
			holy_site = horsegarden
			holy_site = taranton
			holy_site = ryalanar

			doctrine = tenet_honourbound_knighthood
			doctrine = tenet_adaptive
			doctrine = tenet_ritual_celebrations

			#Main Group
			doctrine = doctrine_spiritual_head
			doctrine = doctrine_gender_equal	
			doctrine = doctrine_pluralism_pluralistic
			doctrine = doctrine_clerical_succession_spiritual_fixed_appointment

			#Marriage
			doctrine = doctrine_monogamy
			doctrine = doctrine_divorce_approval
			doctrine = doctrine_bastardry_legitimization
			doctrine = doctrine_consanguinity_cousins

			#Crimes
			doctrine = doctrine_homosexuality_accepted
			doctrine = doctrine_adultery_men_shunned
			doctrine = doctrine_adultery_women_shunned
			doctrine = doctrine_kinslaying_close_kin_crime
			doctrine = doctrine_deviancy_shunned
			doctrine = doctrine_witchcraft_crime

			#Clerical Functions
			doctrine = doctrine_clerical_function_alms_and_pacification
			doctrine = doctrine_clerical_gender_either
			doctrine = doctrine_clerical_marriage_allowed
			doctrine = doctrine_clerical_succession_spiritual_fixed_appointment
	
			#Allow pilgrimages
			doctrine = doctrine_pilgrimage_encouraged
	

			holy_order_names = {	#TODO
				{ name = "holy_order_knights_of_the_white_rose" }
				{ name = "holy_order_knights_of_the_spear" }
				{ name = "holy_order_errant_knights" }
			}

			localization = {
				#HighGod - Adean
				HighGodName = whiterose_court_high_god_name
				HighGodNamePossessive = whiterose_court_high_god_name_possessive
				HighGodNameSheHe = CHARACTER_SHEHE_HE
				HighGodHerselfHimself = CHARACTER_HIMSELF
				HighGodHerHis = CHARACTER_HERHIS_HIS
				HighGodNameAlternate = whiterose_court_high_god_name_alternate
				HighGodNameAlternatePossessive = whiterose_court_high_god_name_alternate_possessive

		
				#FertilityGod
				FertilityGodName = whiterose_court_fertility_god_name
				FertilityGodNamePossessive = whiterose_court_fertility_god_name_possessive
				FertilityGodSheHe = CHARACTER_SHEHE_SHE
				FertilityGodHerHis = CHARACTER_HERHIS_HER
				FertilityGodHerHim = CHARACTER_HERHIM_HER

				#WealthGod
				WealthGodName = whiterose_court_wealth_god_name
				WealthGodNamePossessive = whiterose_court_wealth_god_name_possessive
				WealthGodSheHe = CHARACTER_SHEHE_SHE
				WealthGodHerHis = CHARACTER_HERHIS_HER
				WealthGodHerHim = CHARACTER_HERHIM_HER

				#HouseholdGod
				HouseholdGodName = whiterose_court_household_god_name
				HouseholdGodNamePossessive = whiterose_court_household_god_name_possessive
				HouseholdGodSheHe = CHARACTER_SHEHE_SHE
				HouseholdGodHerHis = CHARACTER_HERHIS_HER
				HouseholdGodHerHim = CHARACTER_HERHIM_HER

				#FateGod
				FateGodName = whiterose_court_fate_god_name
				FateGodNamePossessive = whiterose_court_fate_god_name_possessive
				FateGodSheHe = CHARACTER_SHEHE_HE
				FateGodHerHis = CHARACTER_HERHIS_HIS
				FateGodHerHim = CHARACTER_HERHIM_HIM

				#KnowledgeGod
				KnowledgeGodName = whiterose_court_knowledge_god_name
				KnowledgeGodNamePossessive = whiterose_court_knowledge_god_name_possessive
				KnowledgeGodSheHe = CHARACTER_SHEHE_HE
				KnowledgeGodHerHis = CHARACTER_HERHIS_HIS	
				KnowledgeGodHerHim = CHARACTER_HERHIM_HIM
	
				#WarGod
				WarGodName = whiterose_court_war_god_name
				WarGodNamePossessive = whiterose_court_war_god_name_possessive
				WarGodSheHe = CHARACTER_SHEHE_HE
				WarGodHerHis = CHARACTER_HERHIS_HIS
				WarGodHerHim = CHARACTER_HERHIM_HIM

				#TricksterGod
				TricksterGodName = whiterose_court_trickster_god_name
				TricksterGodNamePossessive = whiterose_court_trickster_god_name_possessive
				TricksterGodSheHe = CHARACTER_SHEHE_SHE
				TricksterGodHerHis = CHARACTER_HERHIS_HER
				TricksterGodHerHim = CHARACTER_HERHIM_HER

				#NightGod
				NightGodName = whiterose_court_night_god_name
				NightGodNamePossessive = whiterose_court_night_god_name_possessive
				NightGodSheHe = CHARACTER_SHEHE_SHE
				NightGodHerHis = CHARACTER_HERHIS_HER
				NightGodHerHim = CHARACTER_HERHIM_HER

				#WaterGod
				WaterGodName = whiterose_court_water_god_name
				WaterGodNamePossessive = whiterose_court_water_god_name_possessive
				WaterGodSheHe = CHARACTER_SHEHE_HE
				WaterGodHerHis = CHARACTER_HERHIS_HIS
				WaterGodHerHim = CHARACTER_HERHIM_HIM


				PantheonTerm = religion_the_gods
				PantheonTermHasHave = pantheon_term_have
				GoodGodNames = {
				whiterose_court_high_god_name
				whiterose_court_water_god_name
				whiterose_court_knowledge_god_name
				whiterose_court_war_god_name
				whiterose_court_household_god_name
				whiterose_court_fate_god_name
				whiterose_court_fertility_god_name
				whiterose_court_health_god_name
				}

				ReligiousSymbol = whiterose_court_religious_symbol

				GHWName = ghw_purification
				GHWNamePlural = ghw_purifications
			}
		}
		castanorian_pantheon = {		#mainstream shit, male dominated though
			color = { 222 222 222 }
			icon = castanorian_pantheon

			holy_site = dragonforge
			holy_site = anbenncost
			holy_site = north_citadel
			holy_site = xhazobains_end

			doctrine = tenet_adaptive	#this is the main pantheon that spread
			doctrine = tenet_legalism	#the faith grew with the empire
			doctrine = tenet_pursuit_of_power	#just classic castanor man

			#Crimes
			doctrine = doctrine_adultery_men_shunned		#obviously more lenient on men
			doctrine = doctrine_witchcraft_crime			#maybe too severe if we tie this with mages

			# localization = {
				# GoodGodNames = {
					# hinduism_high_god_name
					# hinduism_high_god_name_alternate
				# }
			# }
		}

		court_of_adean = { #entebenic main one, knightly virtues
			color = { 0 85 226 }
			icon = court_of_adean

			# holy_site = varanasi
			# holy_site = ayodhya
			# holy_site = mathura
			# holy_site = haridwar
			# holy_site = kanchipuram
			# holy_site = ujjayini
			# holy_site = dwarka

			doctrine = tenet_communal_identity	#just a lencori thing
			doctrine = tenet_monasticism		#knightvibe
			doctrine = tenet_unrelenting_faith	#knight-priests

			doctrine = doctrine_clerical_function_recruitment	#knight-priests
			doctrine = doctrine_clerical_marriage_disallowed	#chaste priesthood

			doctrine = doctrine_homosexuality_crime	#against knightly virtues

			#Crimes

			holy_order_names = {
				{ name = "holy_order_knights_of_the_saltmarch" coat_of_arms = "ho_knights_of_the_saltmarch" }
			}

			# localization = {
				# GoodGodNames = {
					# hinduism_high_god_name
					# hinduism_high_god_name_alternate
				# }
			# }
		}
		
		house_of_minara = {	
			color = { 198 0 116 }
			religious_head = d_minar_temple
			icon = minara

			holy_site = minar
			# holy_site = ayodhya
			# holy_site = mathura
			# holy_site = haridwar
			# holy_site = kanchipuram
			# holy_site = ujjayini
			# holy_site = dwarka


			doctrine = tenet_carnal_exaltation	
			doctrine = tenet_hedonistic			# Minara is goddess of celebrations	
			doctrine = tenet_ritual_celebrations
			
			doctrine = doctrine_deviancy_accepted
			doctrine = doctrine_adultery_men_shunned
			doctrine = doctrine_adultery_women_accepted	#women are free to enjoy themselves, men must not
			doctrine = doctrine_gender_equal
			doctrine = doctrine_polygamy

			doctrine = doctrine_spiritual_head

			doctrine = doctrine_clerical_gender_female_only
			doctrine = doctrine_clerical_marriage_disallowed	#the priestesses must serve all
			doctrine = doctrine_clerical_function_alms_and_pacification
			doctrine = doctrine_clerical_succession_spiritual_fixed_appointment

			#Crimes
			doctrine = doctrine_homosexuality_accepted
			doctrine = doctrine_witchcraft_accepted			

			localization = {
				ReligiousHeadName = minaran_religious_head_title
			}
		}

		small_temple = {		#halfling take
			color = { 145 106 56 }
			icon = small_temple

			# holy_site = varanasi
			# holy_site = ayodhya
			# holy_site = mathura
			# holy_site = haridwar
			# holy_site = kanchipuram
			# holy_site = ujjayini
			# holy_site = dwarka

			doctrine = tenet_communal_identity	#just a halfling thing
			doctrine = tenet_hedonistic			#what about elevenses
			doctrine = tenet_pacifism			#just peaceful halflings

			doctrine = doctrine_bastardry_all	#theres too many halflings to care about it 
			doctrine = doctrine_kinslaying_shunned	#same as above

			doctrine = doctrine_homosexuality_crime	#halflings are very traditional

			doctrine = doctrine_consanguinity_aunt_nephew_and_uncle_niece	#to enable sackville-bagginses vibe
			
			#Crimes

			# localization = {
				# GoodGodNames = {
					# hinduism_high_god_name
					# hinduism_high_god_name_alternate
				# }
			# }
		}

		cult_of_esmaryal = { #unfinished
			color = { 246 182 126 }
			icon = the_dame
			
			doctrine = tenet_sacred_childbirth	#mother
			
			#Crimes
		}

		cult_of_ryala = { #unfinished
			color = { 245 170 200 }
			icon = the_dame
			
			doctrine = tenet_carnal_exaltation	#love
			
			#Crimes
		}

		cult_of_ara = { #unfinished
			color = { 235 211 107 }
			icon = cult_of_ara
			
			#Crimes
		}

		cult_of_balgar = { #unfinished
			color = { 231 231 231 }
			icon = cult_of_balgar
			
			holy_site = silverforge
			holy_site = bal_vroren
			holy_site = bal_hyl
			holy_site = dragonforge
			holy_site = bal_dostan
			
			#Crimes
		}

		southern_cult_of_castellos = { #unfinished
			color = { 181 193 176 }
			icon = castanorian_pantheon
			
			holy_site = north_citadel
			holy_site = xhazobains_end
			
			#Deity
			doctrine = doctrine_patron_deity_castellos
			
			#Crimes
		}
		
		cult_of_nerat = {
			color = { 45 45 45 }
			icon = cult_of_nerat

			holy_site = the_necropolis
			holy_site = nirat # From the good old days
			holy_site = wisphollow
			holy_site = lioncost # From Busilari links
			holy_site = arca_corvur # could be somewhere else in Corvuria really
			
			doctrine = unreformed_faith_doctrine

			doctrine = tenet_communal_identity	#from having been slaves
			doctrine = tenet_corset_burials			#special nerat things, similar to sky burials
			doctrine = tenet_religious_legal_pronouncements
			
			doctrine = doctrine_gender_equal
			doctrine = doctrine_clerical_gender_either
			doctrine = doctrine_clerical_marriage_disallowed

			#Marriage
			doctrine = doctrine_monogamy
			doctrine = doctrine_divorce_disallowed
			doctrine = doctrine_bastardry_none

			#Crimes
			doctrine = doctrine_adultery_men_crime
			doctrine = doctrine_adultery_women_crime
			doctrine = doctrine_kinslaying_close_kin_crime

			#Clerical Functions
			doctrine = doctrine_clerical_function_taxation
			doctrine = doctrine_clerical_succession_temporal_fixed_appointment
		}
		
		court_of_nerat = {
			color = { 15 15 15 }
			icon = cult_of_nerat
			religious_head = d_necropolis # TODO - make a wexonard ruler for this

			holy_site = the_necropolis
			holy_site = anbenncost # replaced nirat
			holy_site = wisphollow
			holy_site = lioncost # From Busilari links
			holy_site = arca_corvur # could be somewhere else in Corvuria really
			
			doctrine = tenet_unrelenting_faith #lorentish/crusader influence
			doctrine = tenet_religious_legal_pronouncements
			doctrine = tenet_esotericism	#wise dudes
			
			
			doctrine = doctrine_gender_equal
			doctrine = doctrine_clerical_gender_either
			doctrine = doctrine_clerical_marriage_disallowed

			#Marriage
			doctrine = doctrine_monogamy

			#Crimes
			doctrine = doctrine_adultery_men_crime
			doctrine = doctrine_adultery_women_crime
			doctrine = doctrine_kinslaying_close_kin_crime

			#Clerical Functions
			doctrine = doctrine_clerical_function_taxation
			doctrine = doctrine_clerical_succession_temporal_fixed_appointment
			
			doctrine = doctrine_spiritual_head
		}
		
		# Must be marked as to found during on_action in mark_faiths_to_found scripted effect
		regent_court = {
			color = { 182 182 182 }
			icon = the_dame
		}
		
		cult_of_falah = { #unfinished
			color = { 36 176 77 }
			icon = falah
			
			
			#Crimes
		}
		
		west_damish = { #unfinished
			color = { 81 234 177 }
			icon = west_damish
			
			
			#Crimes
		}
		vernid_adean = { #unfinished
			color = { 237 25 37 }
			icon = the_dame
			
			
			#Crimes
		}
		luna_damish = { #unfinished
			color = { 168 73 165 }
			icon = the_dame
			
			
			#Crimes
		}
		adenican_adean = { #unfinished
			color = { 56 75 206 }
			icon = the_dame
			
			
			#Crimes
		}
		rohibonic_cult = { #unfinished
			color = { 255 193 86 }
			icon = the_dame
			
			
			#Crimes
		}
		gawedi_temple = { #unfinished
			color = { 65 55 177 }
			icon = the_dame
			
			
			#Crimes
		}
		wexonard_temple = { #unfinished
			color = { 240 33 235 }
			icon = the_dame
			
			
			#Crimes
		}
		iochand_cult = { #unfinished
			color = { 166 98 157 }
			icon = the_dame
			
			doctrine = doctrine_gender_equal
			
			#Crimes
		}
		reverian_cult = { #unfinished
			color = { 166 166 164 }
			icon = the_dame
			
			
			#Crimes
		}
		cult_of_uelos = { #unfinished
			color = { 2 11 128 }
			icon = cult_of_uelos
			
			
			#Crimes
		}
		tefori_damish = { #unfinished
			color = { 0 168 230 }
			icon = the_dame
			
			
			#Crimes
		}
		cult_of_the_lightfather = { #unfinished
			color = { 254 241 4 }
			icon = the_dame
			
			
			#Crimes
		}
		businori_cult = { #unfinished
			color = { 130 126 35 }
			icon = the_dame
			
			
			#Crimes
		}
	}
}
