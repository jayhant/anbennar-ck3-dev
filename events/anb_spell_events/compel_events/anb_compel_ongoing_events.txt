﻿### ONGOING EVENTS FOR COMPEL ###

namespace = anb_compel_ongoing

# Know thy Enemy - Methods of getting to know your target better #
anb_compel_ongoing.1 = {
	type = character_event
	title = anb_compel_ongoing.1.t
	desc = anb_compel_ongoing.1.d

	theme = learning
	
	right_portrait = {
		character = scope:target
		animation = idle
	}

	trigger = {
		NOT = { 
			scope:scheme = { 
				OR = {
					has_scheme_modifier = compel_personal_insights
					has_scheme_modifier = compel_personal_insights_direct
					has_scheme_modifier = compel_personal_insights_strong
				}
			}
		}
	}

	# Meet them personally - More power but less secrecy
	option = {
		name = anb_compel_ongoing.1.a

		stress_impact = {
			shy = minor_stress_impact_gain
			paranoid = medium_stress_impact_gain
		}
		scope:scheme = { add_scheme_modifier = {type = compel_personal_insights_direct} }
	}
	
	# Listen around - Standard low stakes option, made cheaper by being a reveler or gregarious
	option = {
		name = anb_compel_ongoing.1.b
		if = {
			limit = {
				OR = {
					has_trait = lifestyle_reveler
					has_trait = gregarious
				}
			}
			remove_short_term_gold = minor_gold_value
		}
		else = {
			remove_short_term_gold = medium_gold_value
		}
		
		scope:scheme = { add_scheme_modifier = {type = compel_personal_insights} }
	}
	
	# THE MINDRIGGER - A dangerous magic ritual to emulate the target's mind - with potentially lasting effects
	option = {
		name = anb_compel_ongoing.1.c
		
		trigger = {
			scope:owner.learning >= excellent_skill_level	
			NOT = { has_trait = possessed }
		}
		
		duel = {
			skill = learning
			value = high_skill_rating
			30 = {
				desc = anb_compel_ongoing.1.c.success
				
				compare_modifier = {
					value = scope:duel_value
					multiplier = 5
				}
				
				modifier = {
					has_trait = magical_affinity_2
					add = 20
				}
				
				modifier = {
					has_trait = magical_affinity_3
					add = 40
				}
				
				send_interface_toast = {
					title = anb_compel_ongoing.1.c.success_toast
					left_icon = scope:owner
				}
				
				scope:scheme = {
					add_scheme_modifier = {
						type = compel_personal_insights_strong
					}
					add_scheme_progress = 3
				}
			}
			
			300 = {
				desc = anb_compel_ongoing.1.c.failure
				compare_modifier = {
					value = scope:duel_value
					multiplier = -5
				}
				
				send_interface_toast = {
					title = anb_compel_ongoing.1.c.failure_toast
					left_icon = scope:owner
				}
				
				add_trait = possessed_1
				
				# Personality Reversal if applicable - tends to make person more "evil"
				custom_description = {
					text = compel_personality_change
					
					random_list = {
						10 = {
							modifier = {
								factor = 0
								NOT = {has_trait = compassionate}
							}
							modifier = {
								factor = 0
								has_trait = callous
							}
							modifier = {
								factor = 0
									NOT = { scope:target = {has_trait = callous} }
							}
						
							remove_trait = compassionate
							add_trait = callous
						}
						10 = {
							modifier = {
								factor = 0
								NOT = {has_trait = compassionate}
							}
							modifier = {
								factor = 0
								has_trait = sadistic
							}
							modifier = {
								factor = 0
								NOT = { scope:target = {has_trait = sadistic} }
							}
						
							remove_trait = compassionate
							add_trait = sadistic
						}
						10 = {
							modifier = {
								factor = 0
								NOT = {has_trait = calm}
							}
							modifier = {
								factor = 0
								has_trait = wrathful
							}
							modifier = {
								factor = 0
								NOT = { scope:target = {has_trait = wrathful} }
							}
						
							remove_trait = calm
							add_trait = wrathful
						}
						10 = {
							modifier = {
								factor = 0
								NOT = {has_trait = forgiving}
							}
							modifier = {
								factor = 0
								has_trait = vengeful
							}
							modifier = {
								factor = 0
								NOT = { scope:target = {has_trait = vengeful} }
							}
						
							remove_trait = forgiving
							add_trait = vengeful
						}
						10 = {
							modifier = {
								factor = 0
								NOT = {has_trait = just}
							}
							modifier = {
								factor = 0
								has_trait = arbitrary
							}
							modifier = {
								factor = 0
								NOT = { scope:target = {has_trait = arbitrary} }
							}
						
							remove_trait = just
							add_trait = arbitrary
						}
						10 = {
							modifier = {
								factor = 0
								NOT = {has_trait = trusting}
							}
							modifier = {
								factor = 0
								has_trait = paranoid
							}
							modifier = {
								factor = 0
								NOT = { scope:target = {has_trait = paranoid} }
							}
						
							remove_trait = trusting
							add_trait = paranoid
						}
					}
				}	
			}
		}
	}

	# Are they really that bad? - Stop the scheme but lose stress
	option = {
		name = anb_compel_ongoing.1.e
		
		trigger = {
		    has_trait = compassionate
		}
		
		stress_impact = {
			base = major_stress_impact_loss
			lazy = minor_stress_impact_loss
		}
		scope:scheme = { end_scheme = yes }
		
		ai_chance = {
			base = 0
		}
	}
	
}
