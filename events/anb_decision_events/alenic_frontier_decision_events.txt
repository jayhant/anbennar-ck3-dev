﻿namespace = alenic_frontier_decisions

#I will unite the moors
alenic_frontier_decisions.1001 = {
	type = character_event
	title = alenic_frontier_decisions.1001.t
	desc = alenic_frontier_decisions.1001.desc
	theme = war

	immediate = {
		house = {
			add_house_modifier = {
				modifier = westmoors_subjugator_modifier
				years = 100
			}
		}
	}

	option = { #Inform relevant players.
		name = alenic_frontier_decisions.1001.a
		every_player = {
			limit = {
				NOT = { this = root }
				culture = { has_cultural_pillar = heritage_alenic }
			}
			trigger_event = alenic_frontier_decisions.1002
		}
	}
}

alenic_frontier_decisions.1002 = {
	type = character_event
	title = alenic_frontier_decisions.1002.t
	desc = alenic_frontier_decisions.1002.desc.moorman
	theme = war
	right_portrait = {
		character = scope:scoped_ruler
		animation = personality_zealous
	}

	option = {
		name = {
			trigger = {
				capital_province = {
					geographical_region = custom_cannor_westmoors
				}
				NOT = { target_is_liege_or_above = scope:scoped_ruler }
			}
			text = alenic_frontier_decisions.1002.a
		}
		name = {
			trigger = {
				OR = {
					target_is_liege_or_above = scope:scoped_ruler
					NOT = {
						capital_province = {
							geographical_region = custom_cannor_westmoors
						}
					}
				}
			}
			text = alenic_frontier_decisions.1002.b
		}
		if = {
			limit = {
				capital_province = {
					geographical_region = custom_cannor_westmoors
				}
				NOT = { target_is_liege_or_above = scope:scoped_ruler }
			}
			add_opinion = {
				target = scope:scoped_ruler
				modifier = treacherous_invader_opinion
			}
		}
	}
}

# Moorment brought to heel
alenic_frontier_decisions.1003 = {
	type = character_event
	title = alenic_frontier_decisions.1002.t
	desc = alenic_frontier_decisions.1002.desc.moorman
	theme = war
	right_portrait = {
		character = root
		animation = personality_bold
	}
	
	immediate = {
		the_moormen_kneel_decision_effect = yes
	}

	option = {
		name = alenic_frontier_decisions.1003.a
	}
}